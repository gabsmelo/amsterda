module.exports = function( grunt ) {

  grunt.initConfig({
  // Tasks que o Grunt deve executar

		//sass
		sass: {
				dist: {
					files: {
						'assets/css/style.css': 'src/sass/style.scss'
					}
				}
			},

		// css minificado
		cssmin: {
			 	minify: {
					expand: true,
					cwd: 'assets/css/',
					src: ['*.css', '!*.min.css'],
					dest: 'assets/css/',
					ext: '.min.css'
				}
		},

		// Watch
		watch: {
			sass: {
				files: ['src/**/*.scss'],
				tasks: ['default']
			}
		}

  });


  // Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Tarefas executadas
  grunt.registerTask( 'w', [ 'watch' ] );
  grunt.registerTask( 'default', [ 'sass', 'cssmin' ] );

};
